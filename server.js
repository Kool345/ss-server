const express = require("express");
const app = express();
const path = require('path');
const { v4: uuidv4 } = require("uuid");
const ExpressPeerServer = require('peer').ExpressPeerServer;
const cors = require('cors');
const server = require("http").Server(app);
const options = {
    debug: true
}


// Peer

app.use(cors());
app.use('/peerjs', ExpressPeerServer(server, options));
app.use(express.static(__dirname+'/dist/cragin',{ redirect: false }));
app.get('/*',(req,res) => res.sendFile(path.join(__dirname, 'dist/cragin/index.html')));


const io = require("socket.io")(server,{
  transports: ['polling'],
  cors: {
    origin: '*',
  }});
io.on('connect', (socket) => {
  console.log('a user connected', socket.id);


  socket.on('sendName' ,(roomId,name)=>{
    socket.to(roomId).broadcast.emit("receiveName", name);

  })
    socket.on("join-room", (roomId, userId, name) => {
      console.log('a user room', userId,roomId);
      const room = io.sockets.adapter.rooms.get(roomId);
      if(room !== undefined){
        const roomsize = room.size; 
        console.log(`User Connected In Room: ${roomId} - ${roomsize}`);
        if(roomsize < 2 ){
          roomHandler();
      }else{
        socket.emit('notification',{type:'error',payload:'room Full'});
      }
      }else{
        roomHandler();
      }
      
     function roomHandler(){
      socket.join(roomId);

      socket.to(roomId).broadcast.emit("user-connected", {userId: userId,name:name});
        socket.on("message", (message) => {
        io.to(roomId).emit("createMessage", message);
      });
      }

  });
  socket.on('disconnect', () => {

    console.log('a user Disconnected', socket.id);
    
  })
});

// io.on("connection", (socket) => {
//   console.log(socket.id);
//   socket.on("join-room", (roomId, userId) => {
//     socket.join(roomId);
//     socket.to(roomId).broadcast.emit("user-connected", userId);

//     socket.on("message", (message) => {
//       io.to(roomId).emit("createMessage", message);
//     });
//   });
// });

server.listen(process.env.PORT || 3000);
